from django.contrib import admin
from .models import Coupon, Client, Promotion
import logging
import telepot
import random as r



def generate_random_string(length):
    random_string = ''
    random_str_seq = "ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789"
    for i in range(0,length):
        if i % length == 0 and i != 0:
            random_string += '-'
        random_string += str(random_str_seq[r.randint(0, len(random_str_seq) - 1)])
    return random_string

class CouponInline(admin.TabularInline):
	model = Coupon
	extra=0
	can_delete = False

class CouponAdmin(admin.ModelAdmin):
	"""docstring for CouponAdmin"""
	list_display = ['code', 'valid_from','valid_to','discount', 'active']
	list_filter = ['active', 'valid_from', 'valid_to']
	search_fields = ['code']


class ClientAdmin(admin.ModelAdmin):
	"""docstring for CouponAdmin"""
	inlines = [
		CouponInline,
	]
	list_display = ['first_name', 'last_name', 'phone', 'telegram_id', 'active']
	list_filter = ['active', 'phone', 'telegram_id']
	search_fields = ['phone', 'telegram_id']

class PromotionAdmin(admin.ModelAdmin):
	list_display = ['name', 'valid_from', 'valid_to']
	actions = ['coupons_create']

	def coupons_create(self, request, queryset):
		tg_bot = telepot.Bot('647351675:AAGwW_k67_xcddKtwWYVHGdnyfNAGC1f-f4')

		logger = logging.getLogger('telegram.bot')
		clients = Client.objects.all()
		coupon_codes = Coupon.objects.values('code')
		for prom in queryset:
			for client in clients:
				rand = generate_random_string(5)
				while rand in coupon_codes:
					rand = generate_random_string(5)
				cp = Coupon(
					code=rand, 
					client=client,
					promotion=prom, 
					valid_to=prom.valid_to, 
					valid_from=prom.valid_from,
					discount=prom.discount,
					active=True,
					)
				cp.save()
				if client.telegram_id:
					mes = prom.text.replace('::code::', rand)
					tg_bot.sendMessage(client.telegram_id, mes)
	coupons_create.short_description = 'Создать КОДы'


	
admin.site.register(Coupon, CouponAdmin)
admin.site.register(Client, ClientAdmin)
admin.site.register(Promotion, PromotionAdmin)
