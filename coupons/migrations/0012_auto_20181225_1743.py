# Generated by Django 2.1.4 on 2018-12-25 14:43

from django.db import migrations
import phonenumber_field.modelfields


class Migration(migrations.Migration):

    dependencies = [
        ('coupons', '0011_auto_20181225_0653'),
    ]

    operations = [
        migrations.AlterField(
            model_name='client',
            name='phone',
            field=phonenumber_field.modelfields.PhoneNumberField(blank=True, help_text='Nomer Phone', max_length=128, verbose_name='Мобильный №'),
        ),
    ]
