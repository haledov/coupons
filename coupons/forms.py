from bootstrap_datepicker_plus import DatePickerInput
from .models import Client
from django import forms

class ClientForm(forms.ModelForm):
	class Meta:
		model = Client
		fields = ('first_name','last_name','birth_date', 'phone')
		widgets = {
			'birth_date' : DatePickerInput(
				options={
                    "format": "DD/MM/YYYY", # moment date-time format
                    "showClose": True,
                    "showClear": True,
                    "showTodayButton": True,
                    "locale": "ru",
                }
				),
		}
