from django.shortcuts import render, get_object_or_404, redirect
from django.utils import timezone
from django.template import Context, loader
from .models import Coupon
from .forms import ClientForm

def index(request):
	return render(request,'index.html')

def show(request):
	query = request.POST.get('q', None)
	try:
		c = Coupon.objects.get(code=query)
		if c.active:
			ctxt = {'coup': c }
			return render(request,'show.html',ctxt)	
		elif not c.active:
			context = {
			'message' : 'КОД уже активирован!!!'
			}
			return render(request, 'messages.html', context)
	except:
		context = {
		'message' : 'Такого КОДа не существует!'
		}
		return render(request, 'messages.html', context)
	

def activate(request):
	query = request.POST.get('q', None)
	c = Coupon.objects.get(code=query)
	c.active = False
	c.save()
	context = {
		'message' : 'КОД успешно активирован!'
	}
	return render(request, 'messages.html', context)

	

def signup(request):
	if request.method == 'POST':
		form = ClientForm(request.POST)
		if form.is_valid():

			form.save()
			context = {
			'message' : 'Номер успешно зарегистрирован!'
		}
		return render(request, 'messages.html', context)
	else:
		form = ClientForm()
		return render(request,'signup.html', {'form':form})