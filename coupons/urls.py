from django.conf.urls import url
from .import views
urlpatterns = [
#url(r'^apply/$',views.coupon_apply, name='apply'),
url(r'^$', views.index),
url(r'^index/', views.index, name='index'),
url(r'^show/', views.show, name='show'),
url(r'^activate/', views.activate, name='activate'),
url(r'^signup/', views.signup, name='signup'),
]