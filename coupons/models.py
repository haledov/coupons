from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator
from phonenumber_field.modelfields import PhoneNumberField

# Create your models here.
class Coupon(models.Model):
	code = models.CharField(max_length=50,
							unique=True)
	valid_from = models.DateTimeField()
	valid_to = models.DateTimeField()
	discount = models.IntegerField(validators=[MinValueValidator(0),
												MaxValueValidator(100)])
	active = models.BooleanField()
	client = models.ForeignKey(
		'Client',
		null=True,
		blank=True,
		on_delete=models.CASCADE,
		)
	shops = models.ManyToManyField('Shop')
	promotion = models.ForeignKey(
		'Promotion',
		null=True,
		blank=True,
		on_delete=models.CASCADE,
		)

	def __str__(self):
		return self.code


class Client(models.Model):
	first_name = models.CharField('Имя', max_length=50)
	last_name = models.CharField('Фамилия', max_length=50, blank=True, null=True)
	phone = PhoneNumberField(verbose_name='Мобильный', null=True, blank=True, unique=True)
	email = models.CharField('E-mail', max_length=50, blank=True, null=True)
	birth_date = models.DateField('День рожденья', null=True, blank=True)
	active = models.BooleanField(default=True)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)
	telegram_id = models.IntegerField(null=True, blank=True, unique=True)
	def __str__(self):
		return self.first_name

	def is_active(self):
		return self.active

class Shop(models.Model):
	name = models.CharField(max_length=50)
	def __str__(self):
		return self.name
		
class Promotion(models.Model):
	name = models.CharField('Название', max_length=255, blank=False)
	discount = models.IntegerField(validators=[MinValueValidator(0),
												MaxValueValidator(100)])
	description = models.TextField('Описание')
	text = models.TextField('Сообщение', blank=False)
	shops = models.ManyToManyField('Shop')
	valid_from = models.DateTimeField()
	valid_to = models.DateTimeField()
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	def __str__(self):
		return self.name

