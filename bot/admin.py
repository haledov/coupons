from django.contrib import admin
from .models import Client, Message, Apireply

class ApireplyAdmin(admin.ModelAdmin):
    list_display = ['sender', 'date', 'text']
    list_filter = ['sender', 'date']

# Register your models here.
admin.site.register(Message)
admin.site.register(Apireply, ApireplyAdmin)