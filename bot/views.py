import time
import apiai, json
import logging
from pprint import pprint
import telepot
from django.template.loader import render_to_string
from django.http import HttpResponseForbidden, HttpResponseBadRequest, JsonResponse
from django.views.generic import View
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from django.conf import settings
from coupons.models import Client, Shop, Coupon
from telepot.namedtuple import ReplyKeyboardMarkup, KeyboardButton
from telepot.namedtuple import InlineKeyboardMarkup, InlineKeyboardButton
from .models import Apireply


tg_bot = telepot.Bot('647351675:AAGwW_k67_xcddKtwWYVHGdnyfNAGC1f-f4')

logger = logging.getLogger('telegram.bot')


tg_commands = {}
 # return cmd, params
def parse_cmd(cmd_string):
	text_split = cmd_string.split()
	return text_split[0], text_split[1:]

def add_command(cmd, func):
	global tg_commands
	tg_commands[cmd] = func

def remove_command(cmd):
	global tg_commands
	del tg_commands[cmd]

def send_text(chat_id, reply_text, repmark=None):
    global tg_bot
    keyboard=ReplyKeyboardMarkup(keyboard=[[KeyboardButton(text="First Button")]])
    markup = ReplyKeyboardMarkup(keyboard=[
                [KeyboardButton(text='/map'), KeyboardButton(text='/help'), KeyboardButton(text='/mycodes')],
            ])
    mup = InlineKeyboardMarkup(inline_keyboard=[
                     [dict(text='Telegram URL', url='https://core.telegram.org/')],
                     [InlineKeyboardButton(text='Callback - show notification', callback_data='notification')],
                     [dict(text='Callback - show alert', callback_data='alert')],
                     [InlineKeyboardButton(text='Callback - edit message', callback_data='edit')],
                     [dict(text='Switch to using bot inline', switch_inline_query='initial query')],
                 ])

    tg_bot.sendMessage(chat_id=chat_id, text=reply_text, reply_markup=markup)

def send_reply(chat_id, msg_text):
    request = apiai.ApiAI('5bbd3378f6c2490ca6e37e201a46f3b2').text_request() # Токен API к Dialogflow
    request.lang = 'ru' # На каком языке будет послан запрос
    request.session_id = 'BbAIBot' # ID Сессии диалога (нужно, чтобы потом учить бота)
    request.query = msg_text # Посылаем запрос к ИИ с сообщением от юзера
    responseJson = json.loads(request.getresponse().read().decode('utf-8'))
    response = responseJson['result']['fulfillment']['speech'] # Разбираем JSON и вытаскиваем ответ
    # Если есть ответ от бота - присылаем юзеру, если нет - бот его не понял
    if response:
        send_text(chat_id, response)
    else:
        send_text(chat_id, 'Я вас не совсем понял')

# Если есть сообщение
def on_message(content_type, chat_type, chat_id, message):
    global tg_bot 
    
    
    if content_type == "text":
        msg_text = message['text']
        chat_id = message['chat']['id']

        if msg_text[0] == '/':
            cmd, params = parse_cmd(msg_text)
            try:
                tg_commands[cmd](chat_id, params, message)
            except KeyError:
                send_text(chat_id, "Вот это я не понял: {cmd}".format(cmd=cmd))
        else:
            send_reply(chat_id, msg_text)
    else:
        send_text(chat_id, "Не тот тип сообщения")
			
		
def cmd_elegant(chat_id, params, message):
    shops = Shop.objects.all()
    mesaga = ''
    for shop in shops:
        mesaga = mesaga + '\n' + shop.name
    send_reply(chat_id, mesaga)

def cmd_help(chat_id, params, message):
    send_reply(chat_id, render_to_string('help.md'))    

def cmd_start(chat_id, params, message):
    cl, created = Client.objects.get_or_create(
        telegram_id = message['from']['id'],
        first_name = message['from']['first_name']
        )
    
    #tg_bot.sendMessage(chat_id, "Здраствуй <b>{text}</b> !".format(text=cl.name))
    send_reply(chat_id, 'Привет *' + cl.first_name+'*', reply_markup=markup)
    time.sleep(2)
    send_reply(chat_id, render_to_string('help.md'), reply_markup=markup)
    time.sleep(2)
    send_reply(chat_id, '*Ты можешь задавть мне вопросы если захочешь. А я на них отвечку если смогу!*', reply_markup=markup)

def cmd_update(chat_id, params, message):
    cl, created = Client.objects.get_or_create(
        telegram_id = message['from']['id'],
        defaults = {
            'first_name' : message['from']['first_name'], 
            #'last_name' : message['from']['username'], 
            }
        )
    if not created:
        cl.first_name = message['from']['first_name']
        #cl.last_name = message['from']['last_name'] if message['from']['last_name'] else message['from']['username']
        cl.save()
    tg_bot.sendMessage(chat_id, 'Спасибо '+message['from']['first_name'])

def cmd_help(chat_id, params, message):
    tg_bot.sendMessage(chat_id, render_to_string('help.md'))

def cmd_map(chat_id, params, message):
    tg_bot.sendPhoto(chat_id, open(settings.STATIC_ROOT+"calend/map.png", 'rb'));
    #tg_bot.sendMessage(chat_id, render_to_string('help.md'))    

def cmd_coupons(chat_id, params, message):
    client = Client.objects.get(telegram_id__exact=chat_id)
    list_coupons = Coupon.objects.filter(client__id=client.id, active=True)
    if list_coupons:
        tg_bot.sendMessage(chat_id, render_to_string('coupons.md', {'coupons': list_coupons}))
    else:
        tg_bot.sendMessage(chat_id, 'Пока купонов нет ((')
    #tg_bot.sendMessage(chat_id, "Здраствуй <b>{text}</b> !".format(text=cl.name))

class CommandReceiveView(View):
    def post(self, request, bot_token):
        global tg_bot
        if bot_token != '647351675:AAGwW_k67_xcddKtwWYVHGdnyfNAGC1f-f4':
           return HttpResponseForbidden('Invalid token')
        #сброс ошибок 
        #return JsonResponse({}, status=200)        
        
        raw = request.body.decode('utf-8')
        logger.info(raw)

        try:
            payload = json.loads(raw)
            content_type, chat_type, chat_id = telepot.glance(payload['message'])
        except ValueError:
            return HttpResponseBadRequest('Invalid request body')
        else:
            add_command("/start", cmd_start)
            add_command("/update", cmd_update)
            add_command("/elegant", cmd_elegant)
            add_command("/help", cmd_help)
            add_command("/map", cmd_map)
            add_command("/mycodes", cmd_coupons)
            on_message(content_type, chat_type, chat_id , payload['message'])
            #chat_id = payload['message']['chat']['id']
            #cmd = payload['message'].get('text')  # command
            #ser = payload['message']['from']
            #show_keyboard = {'keyboard': [['Yes','No'], ['Maybe','Maybe not']]}
            #func = commands.get(cmd.split()[0].lower())
            #if func:
            #    TelegramBot.sendMessage(chat_id, func(), parse_mode='Markdown', reply_markup=show_keyboard)
            #else:
            #    TelegramBot.sendMessage(chat_id, 'мне нечего сказать')
        sender = Client.objects.get(telegram_id__exact = payload['message']['from']['id'])
        repl = Apireply(
            #update_id = message['update_id'],
            text = payload['message']['text'],
            json = payload['message'],
            chat_id = chat_id,
            sender = sender
            )
        repl.save()
        return JsonResponse({}, status=200)

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(CommandReceiveView, self).dispatch(request, *args, **kwargs)