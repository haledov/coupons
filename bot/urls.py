
from django.conf.urls import url

from .views import CommandReceiveView

urlpatterns = [
    url(r'^hook/(?P<bot_token>.+)/$', CommandReceiveView.as_view(), name='command'),
]