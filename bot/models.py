import datetime
from django.utils import timezone
from django.db import models
from coupons.models import Client
import telepot


tg_bot = telepot.Bot('647351675:AAGwW_k67_xcddKtwWYVHGdnyfNAGC1f-f4')

# Create your models here.
class Message(models.Model):
    update_id       = models.IntegerField(unique=True)
    text            = models.TextField(max_length=4096)
    date            = models.DateTimeField(default=timezone.now)
    sender          = models.ForeignKey(Client, on_delete=models.CASCADE)

    def save(self, *args, **kwargs):
        tg_bot.sendMessage(chat_id=self.sender.telegram_id, text=self.text)
        super(Message, self).save(*args, **kwargs)

    def __str__(self):
        return f'{self.text}'
class Apireply(models.Model):
    update_id       = models.IntegerField(blank=True, null=True)
    text            = models.TextField(max_length=4096)
    date            = models.DateTimeField(default=timezone.now)
    chat_id         = models.IntegerField()
    sender          = models.ForeignKey(Client, on_delete=models.CASCADE)
    json            = models.TextField(max_length=4096)