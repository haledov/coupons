# Generated by Django 2.1.4 on 2018-12-31 07:53

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('bot', '0004_auto_20181231_1050'),
    ]

    operations = [
        migrations.AlterField(
            model_name='apireply',
            name='sender',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='coupons.Client'),
        ),
    ]
